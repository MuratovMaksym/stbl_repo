﻿using SWS.Common;
using SWS.WebApp.Areas.Admin.Models.BlockModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Mappers.BlockMappers
{
    public static class BlockMapper
    {
        public static List<BlockModel> ToModel(this IEnumerable<Block> entities)
        {
            List<BlockModel> modelItems = new List<BlockModel>();
            foreach (var entity in entities)
            {
                modelItems.Add(MapEntityToModel(entity));
            }
            return modelItems;
        }

        private static BlockModel MapEntityToModel(Block entity)
        {
            BlockModel model = new BlockModel();
            model.AltKey = entity.AltKey;
            model.Id = entity.Id;

            foreach (var item in entity.LocalizedBlockValues)
            {
                model.LocalizedBlockValues.Add(new LocalizedBlockValueModel
                { 
                    Id = item.Id,
                    BlockId = item.BlockId,
                    LanguageId = item.LanguageId,
                    Title = item.Title
                });
            }
            return model;
        }

        public static Block ToEntity(this CreateBlockModel model)
        {
            return null;
        }

        public static EditBlockModel ToModel(this Block entity)
        {
            return null;
        }

        public static Block ToEntity(this EditBlockModel model)
        {
            return null;
        }
    }
}