﻿using SWS.Common;
using SWS.WebApp.Areas.Admin.Models.LocalizationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Mappers.LocalizationMapper
{
    public static class LocalizedMapper
    {
        public static List<LocalizationModel> ToModel(this IEnumerable<Language> entities)
        {
            List<LocalizationModel> modelItems = new List<LocalizationModel>();
            foreach (var entity in entities)
            {
                modelItems.Add(MapEntityToModel(entity));
            }
            return modelItems;
        }

        private static LocalizationModel MapEntityToModel(Language entity)
        {
            return new LocalizationModel
            {
                Id = entity.Id,
                LanguageKey = entity.LanguageKey,
                Description = entity.Description,
                Icon = entity.Icon
            };
        }

        public static Language ToEntity(this CreateLocalizationModel model)
        {
            return new Language
            {
                Id = 0,
                LanguageKey = model.LanguageKey,
                Description = model.Description,
                Icon = model.Icon
            };
        }

        public static EditLocalizationModel ToModel(this Language entity)
        {
            return new EditLocalizationModel
            {
                Id = entity.Id,
                LanguageKey = entity.LanguageKey,
                Description = entity.Description,
                Icon = entity.Icon
            };
        }

        public static Language ToEntity(this EditLocalizationModel model)
        {
            return new Language
            {
                Id = model.Id,
                LanguageKey = model.LanguageKey,
                Description = model.Description,
                Icon = model.Icon
            };
        }
    }
}