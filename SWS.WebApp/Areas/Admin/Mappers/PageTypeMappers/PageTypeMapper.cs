﻿using SWS.Common;
using SWS.WebApp.Areas.Admin.Models.PageTypeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Mappers.PageTypeMappers
{
    public static class PageTypeMapper
    {
        public static List<PageTypeModel> ToModel(this IEnumerable<PageType> entities)
        { 
            List<PageTypeModel> modelItems = new List<PageTypeModel>();
            foreach(var entity in entities)
            {
                modelItems.Add(MapEntityToModel(entity));
            }
            return modelItems;
        }

        private static PageTypeModel MapEntityToModel(PageType entity)
        {
            return new PageTypeModel { 
                Id = entity.Id,
                Name = entity.Name,
                ShortDescription = entity.ShortDescription
            };
        }

        public static PageType ToEntity(this CreatePageTypeModel model)
        {
            return new PageType { 
                Name = model.Name,
                ShortDescription = model.ShortDescription
            };
        }

        public static EditPageTypeModel ToModel(this PageType entity)
        {
            return new EditPageTypeModel
            {
                Id = entity.Id,
                Name = entity.Name,
                ShortDescription = entity.ShortDescription
            };
        }

        public static PageType ToEntity(this EditPageTypeModel model)
        {
            return new PageType
            {
                Id = model.Id,
                Name = model.Name,
                ShortDescription = model.ShortDescription
            };
        }
    }
}