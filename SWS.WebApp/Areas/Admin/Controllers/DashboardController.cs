﻿using SWS.Common;
using SWS.Services.PageTypeServices;
using SWS.WebApp.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SWS.WebApp.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IPageTypeService pageTypeService;

        public DashboardController()
        {
            this.pageTypeService = new PageTypeService(new SWSContext());
        }

        //
        // GET: /Admin/Dashboard/
        public ActionResult Index()
        {
            pageTypeService.Add(new Common.PageType() { 
                Name = "Test",
                ShortDescription = "short description"
            });
            return View();
        }
	}
}