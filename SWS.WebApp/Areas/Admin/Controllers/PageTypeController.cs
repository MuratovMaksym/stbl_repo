﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SWS.Common;
using SWS.WebApp.Areas.Admin.Mappers.PageTypeMappers;
using SWS.WebApp.Areas.Admin.Models.PageTypeModels;
using SWS.Services.PageTypeServices;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace SWS.WebApp.Areas.Admin.Controllers
{
    public class PageTypeController : Controller
    {
        private readonly IPageTypeService pageTypeService;

        public PageTypeController()
        {
            SWSContext db = new SWSContext();
            pageTypeService = new PageTypeService(db);
        }

        // GET: /Admin/PageType/
        public ActionResult Index()
        {
            var pageTypeModelItems = pageTypeService.GetAll().ToModel();
            return View(pageTypeModelItems);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, CreatePageTypeModel model)
        {
            if (ModelState.IsValid)
            {
                pageTypeService.Add(model.ToEntity());
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, EditPageTypeModel model)
        {
            if (ModelState.IsValid)
            {
                pageTypeService.Update(model.ToEntity());
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, PageTypeModel model)
        {
            if(ModelState.IsValid)
            {
                pageTypeService.Delete(model.Id);
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
    }
}
