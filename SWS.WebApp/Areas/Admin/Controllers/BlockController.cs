﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using SWS.WebApp.Areas.Admin.Mappers.BlockMappers;
using Kendo.Mvc.UI;
using SWS.Common;
using SWS.Services.BlockServices;
using SWS.WebApp.Areas.Admin.Models.BlockModels;
using SWS.Services.LanguageServices;
using SWS.WebApp.Areas.Admin.Mappers.LocalizationMapper;

namespace SWS.WebApp.Areas.Admin.Controllers
{
    public class BlockController : Controller
    {
        private readonly IBlockService blockService;
        private readonly ILanguageService languageService;

        public BlockController()
        {
            SWSContext db = new SWSContext();
            blockService = new BlockService(db);
            languageService = new LanguageService(db);
        }

        // GET: /Admin/PageType/
        public ActionResult Index()
        {
            var pageTypeModelItems = blockService.GetAll().ToList().ToModel();
            return View(pageTypeModelItems);
        }

        [ChildActionOnly]
        public ActionResult LocalizationBlockEditor(int index, LocalizedBlockValueModel model)
        {
            ViewBag.Index = index;
            return PartialView(model);
        }

        public ActionResult Create()
        {
            var languages = languageService.GetAll();
            if (languages != null && languages.Count > 0)
            {
                var langugeModelItems = languages.ToModel();
                ViewBag.Languages = languageService.GetAll().ToList().ToModel();
                CreateBlockModel model = new CreateBlockModel();
                PrepareCreateModel(model);
                return View(model);
            }
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(CreateBlockModel model)
        {
            if (ModelState.IsValid)
            {
                blockService.Add(model.ToEntity());
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, EditBlockModel model)
        {
            if (ModelState.IsValid)
            {
                blockService.Update(model.ToEntity());
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, EditBlockModel model)
        {
            if (ModelState.IsValid)
            {
                blockService.Delete(model.Id);
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        private void PrepareCreateModel(CreateBlockModel model)
        {
            var languages = languageService.GetAll();
            foreach (var lang in languages)
            {
                model.LocalizedBlockValues.Add(new LocalizedBlockValueModel { 
                    LanguageId = lang.Id,
                    LanguageKey = lang.LanguageKey
                });
            }
        }
    }
}