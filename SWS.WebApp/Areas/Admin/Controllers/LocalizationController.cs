﻿using SWS.Common;
using SWS.Services.LanguageServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SWS.WebApp.Areas.Admin.Mappers.LocalizationMapper;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using SWS.WebApp.Areas.Admin.Models.LocalizationModels;
using System.IO;

namespace SWS.WebApp.Areas.Admin.Controllers
{
    public class LocalizationController : Controller
    {
        private readonly ILanguageService languageService;
        public LocalizationController()
        {
            SWSContext db = new SWSContext();
            languageService = new LanguageService(db);
        }

        // GET: /Admin/PageType/
        public ActionResult Index()
        {
            var pageTypeModelItems = languageService.GetAll().ToList().ToModel();
            return View(pageTypeModelItems);
        }

        public ActionResult Create()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(CreateLocalizationModel model, HttpPostedFileBase attachment)
        {
            if (attachment != null)
            {
                var fileName = Path.GetFileName(attachment.FileName);
                var destinationPath = Path.Combine(Server.MapPath("~/Content/SWS/Localizations"), fileName);
                attachment.SaveAs(destinationPath);
                model.Icon = destinationPath;
            }

            if (ModelState.IsValid)
            {
                languageService.Add(model.ToEntity());
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            Language entity = languageService.Get(id);
            return View(entity.ToModel());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, EditLocalizationModel model, HttpPostedFileBase attachment)
        {
            if (attachment != null)
            {
                var fileName = Path.GetFileName(attachment.FileName);
                var destinationPath = Path.Combine(Server.MapPath("~/Content/SWS/Localizations"), fileName);
                attachment.SaveAs(destinationPath);

                System.IO.File.Delete(model.Icon); // remove previous file
                model.Icon = destinationPath;
            }

            if (ModelState.IsValid)
            {
                languageService.Update(model.ToEntity());
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                Language language = languageService.Get(id);
                System.IO.File.Delete(language.Icon); // remove file
                languageService.Delete(language.Id);
            }
            return RedirectToAction("Index");
        }
    }
}