﻿using FluentValidation.Attributes;
using SWS.WebApp.Areas.Admin.Validators.LocalizationValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Models.LocalizationModels
{
    [Validator(typeof(CreateLocalizationValidator))] 
    public class CreateLocalizationModel
    {
        public string LanguageKey { get; set; }

        public string Description { get; set; }

        public string Icon { get; set; }
    }
}