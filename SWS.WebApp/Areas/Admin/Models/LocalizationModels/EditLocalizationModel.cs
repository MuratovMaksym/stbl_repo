﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Models.LocalizationModels
{
    public class EditLocalizationModel
    {
        public int Id { get; set; }

        public string LanguageKey { get; set; }

        public string Description { get; set; }

        public string Icon { get; set; }
    }
}