﻿using FluentValidation.Attributes;
using SWS.WebApp.Areas.Admin.Validators.PageTypeValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Models.PageTypeModels
{
    [Validator(typeof(EditPageTypeValidator))]    
    public class EditPageTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
    }
}