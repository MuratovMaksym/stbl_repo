﻿using FluentValidation.Attributes;
using SWS.WebApp.Areas.Admin.Validators.BlockValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Models.BlockModels
{
    [Validator(typeof(EditBlockValidator))]
    public class EditBlockModel
    {
        public EditBlockModel()
        {
            this.LocalizedBlockValues = new List<LocalizedBlockValueModel>();
        }
        public int Id { get; set; }
        public string AltKey { get; set; }
        public List<LocalizedBlockValueModel> LocalizedBlockValues { get; set; }

    }
}