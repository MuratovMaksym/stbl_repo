﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Models.BlockModels
{
    public class LocalizedBlockValueModel
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
        public int BlockId { get; set; }

        public string LanguageKey { get; set; }
        public string Title { get; set; }
    }
}