﻿using FluentValidation.Attributes;
using SWS.WebApp.Areas.Admin.Mappers.BlockMappers;
using SWS.WebApp.Areas.Admin.Validators.BlockValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Models.BlockModels
{
    [Validator(typeof(CreateBlockValidator))] 
    public class CreateBlockModel
    {
        public CreateBlockModel()
        {
            this.LocalizedBlockValues = new List<LocalizedBlockValueModel>();
        }
        public string AltKey { get; set; }
        public List<LocalizedBlockValueModel> LocalizedBlockValues { get; set; }

    }
}