﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Models.BlockModels
{
    public class BlockModel
    {
        public BlockModel()
        {
            this.LocalizedBlockValues = new List<LocalizedBlockValueModel>();
        }
        public int Id { get; set; }
        public string AltKey { get; set; }
        public List<LocalizedBlockValueModel> LocalizedBlockValues { get; set; }
    }    
}