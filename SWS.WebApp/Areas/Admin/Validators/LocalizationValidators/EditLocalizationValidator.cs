﻿using FluentValidation;
using SWS.WebApp.Areas.Admin.Models.LocalizationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Validators.LocalizationValidators
{
    public class EditLocalizationValidator : AbstractValidator<EditLocalizationModel>
    {
        public EditLocalizationValidator()
        {
            RuleFor(x => x.Id).NotNull().WithMessage("*");
            RuleFor(x => x.LanguageKey).Length(0, 15).NotNull().WithMessage("*");
            RuleFor(x => x.Icon).Length(0, 2000).NotNull().WithMessage("*");
            RuleFor(x => x.Description).Length(0, 60).NotNull().WithMessage("*");
        }
    }
}