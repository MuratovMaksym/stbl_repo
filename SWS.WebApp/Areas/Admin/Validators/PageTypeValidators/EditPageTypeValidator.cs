﻿using FluentValidation;
using SWS.WebApp.Areas.Admin.Models.PageTypeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Validators.PageTypeValidators
{
    public class EditPageTypeValidator : AbstractValidator<EditPageTypeModel>
    {
        public EditPageTypeValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.Name).Length(0, 150).NotNull();
            RuleFor(x => x.ShortDescription).Length(0, 500).NotNull();
        }
    }
}