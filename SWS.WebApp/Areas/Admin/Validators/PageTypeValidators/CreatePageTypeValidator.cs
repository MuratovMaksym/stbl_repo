﻿using FluentValidation;
using SWS.WebApp.Areas.Admin.Models.PageTypeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Validators.PageTypeValidators
{
    public class CreatePageTypeValidator : AbstractValidator<CreatePageTypeModel>
    {
        public CreatePageTypeValidator()
        {
            RuleFor(x => x.Name).Length(0, 150).NotNull().WithMessage("*");
            RuleFor(x => x.ShortDescription).Length(0, 500).NotNull().WithMessage("*");
        }
    }
}