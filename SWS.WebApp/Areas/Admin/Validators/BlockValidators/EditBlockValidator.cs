﻿using FluentValidation;
using SWS.WebApp.Areas.Admin.Models.BlockModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWS.WebApp.Areas.Admin.Validators.BlockValidators
{
    public class EditBlockValidator : AbstractValidator<EditBlockModel>
    {
        public EditBlockValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.AltKey).Length(0, 120).NotNull().WithMessage("*");
        }
    }
}