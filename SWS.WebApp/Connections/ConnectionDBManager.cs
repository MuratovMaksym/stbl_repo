﻿using SWS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SWS.WebApp.Connections
{
    public class ConnectionDBManager
    {
        public static SWSContext GetContext()
        {
            SWSContext sws = new SWSContext();
            sws.Database.Connection.ConnectionString = WebConfigurationManager.ConnectionStrings["SWSContext"].ConnectionString;
            return sws;
        }
    }
}