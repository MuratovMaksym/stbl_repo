﻿using SWS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Services.LanguageServices
{
    public class LanguageService : ILanguageService
    {
        private readonly ICommonService<Language> _commonService;
        private readonly SWSContext _swsContext;

        public LanguageService(SWSContext dbContext)
        {
            _commonService = new CommonService<Language>(dbContext);
            _swsContext = dbContext;
        }
        public bool Add(Common.Language language)
        {
            return _commonService.Add(language);
        }
        public bool Update(Common.Language language)
        {
            return _commonService.Update(language);
        }
        public IList<Common.Language> GetAll()
        {
            return _commonService.GetAll();
        }
        public IList<Common.Language> GetAll(int skip, int take, out int totalCount)
        {
            return _commonService.GetAll(skip, take, out totalCount);
        }
        public Common.Language Get(int id)
        {
            return _commonService.Get(id);
        }
        public bool Delete(int id)
        {
            Language language = _commonService.Get(id);
            if (language != null)
            {
                return _commonService.Delete(language);
            }
            return false;
        }

        public Language GetDefaultLAnguage()
        {
            return _commonService.GetAll().FirstOrDefault(x => x.AsDefault);
        }
    }
}
