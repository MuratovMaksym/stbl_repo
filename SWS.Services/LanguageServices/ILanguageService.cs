﻿using SWS.Common;
using System.Collections.Generic;
namespace SWS.Services.LanguageServices
{
    public interface ILanguageService
    {
        bool Add(Language language);

        bool Update(Language language);

        IList<Language> GetAll();

        IList<Language> GetAll(int skip, int take, out int totalCount);

        Language Get(int id);

        bool Delete(int id);

        Language GetDefaultLAnguage();
    }
}
