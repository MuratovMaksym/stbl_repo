﻿using System.Collections.Generic;
using SWS.Common;
using System.Linq;
using SWS.Services.LanguageServices;

namespace SWS.Services.BlockServices
{
    public class BlockService : IBlockService
    {
        private readonly ICommonService<Block> _blockService;
        private readonly SWSContext _swsContext;

        public BlockService(SWSContext dbContext)
        {
            _blockService = new CommonService<Block>(dbContext);
            _swsContext = dbContext;
        }

        public IList<Block> GetAll()
        {
            return _blockService.GetAll();
        }

        public IList<Block> GetAll(int skip, int take, out int totalCount)
        {
            return _blockService.GetAll(skip, take, out totalCount);
        }

        public Block Get(int id)
        {
            return _blockService.Get(id);
        }

        public bool Add(Block block)
        {
            return _blockService.Add(block);
        }

        public bool Update(Block block)
        {
            return _blockService.Update(block);
        }

        public bool Delete(int id)
        {
            Block block = _blockService.Get(id);
            if (block != null)
            {
                return _blockService.Delete(block);
            }
            return false;
        } 
    }
}
