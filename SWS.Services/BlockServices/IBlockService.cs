﻿using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.BlockServices
{
    public interface IBlockService
    {
        bool Add(Block block);

        bool Update(Block block);

        IList<Block> GetAll();

        IList<Block> GetAll(int skip, int take, out int totalCount);

        Block Get(int id);

        bool Delete(int id);
    }
}
