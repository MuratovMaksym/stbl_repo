﻿using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.BlockItemServices
{
    public class BlockItemService : IBlockItemService
    {
        private readonly ICommonService<BlockItem> _commonService;
        private readonly SWSContext _swsContext;

        public BlockItemService(SWSContext dbContext)
        {
            _commonService = new CommonService<BlockItem>(dbContext);
            _swsContext = dbContext;
        }

        public IList<BlockItem> GetAll()
        {
            return _commonService.GetAll();
        }

        public IList<BlockItem> GetAll(int skip, int take, out int totalCount)
        {
            return _commonService.GetAll(skip, take, out totalCount);
        }

        public BlockItem Get(int id)
        {
            return _commonService.Get(id);
        }

        public bool Add(BlockItem blockItem)
        {
            return _commonService.Add(blockItem);
        }

        public bool Update(BlockItem blockItem)
        {
            return _commonService.Update(blockItem);
        }

        public bool Delete(int id)
        {
            BlockItem blockItem = _commonService.Get(id);
            if (blockItem != null)
            {
                return _commonService.Delete(blockItem);
            }
            return false;
        }
    }
}