﻿using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.BlockItemServices
{
    public interface IBlockItemService
    {
        bool Add(BlockItem blockItem);

        bool Update(BlockItem blockItem);

        IList<BlockItem> GetAll();

        IList<BlockItem> GetAll(int skip, int take, out int totalCount);

        BlockItem Get(int id);

        bool Delete(int id);
    }
}
