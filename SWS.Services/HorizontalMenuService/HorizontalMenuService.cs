using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.HorizontalMenuService
{
    public class HorizontalMenu : IHorizontalMenu
    {
        private readonly ICommonService<HorizontalMenu> _commonService;
        private readonly SWSContext _swsContext;

        public HorizontalMenu(SWSContext dbContext)
        {
            _commonService = new CommonService<HorizontalMenu>(dbContext);
            _swsContext = dbContext;
        }

        public bool Add(HorizontalMenu horizontalMenu)
        {
            return _commonService.Add(horizontalMenu);
        }

        public bool Update(HorizontalMenu horizontalMenu)
        {
            return _commonService.Update(horizontalMenu);
        }

        public IList<HorizontalMenu> GetAll()
        {
            return _commonService.GetAll();
        }

        public IList<HorizontalMenu> GetAll(int skip, int take, out int totalCount)
        {
            return _commonService.GetAll(skip, take, out totalCount);
        }

        public HorizontalMenu Get(int id)
        {
            return _commonService.Get(id);
        }

        public bool Delete(int id)
        {
            HorizontalMenu horizontalMenu = _commonService.Get(id);
            if (horizontalMenu != null)
            {
                return _commonService.Delete(horizontalMenu);
            }
            return false;
        }
    }
}