using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.HorizontalMenuService
{
    public interface IHorizontalMenu
    {
        bool Add(HorizontalMenu pageType);

        bool Update(HorizontalMenu pageType);

        IList<HorizontalMenu> GetAll();

        IList<HorizontalMenu> GetAll(int skip, int take, out int totalCount);

        HorizontalMenu Get(int id);

        bool Delete(int id);
    }
}
