﻿using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.PageServices
{
    public class PageService : IPageService
    {
        private readonly ICommonService<Page> _commonService;
        private readonly SWSContext _swsContext;

        public PageService(SWSContext dbContext)
        {
            _commonService = new CommonService<Page>(dbContext);
            _swsContext = dbContext;
        }

        public bool Add(Page page)
        {
            return _commonService.Add(page);
        }

        public bool Update(Page page)
        {
            return _commonService.Update(page);
        }

        public IList<Page> GetAll()
        {
            return _commonService.GetAll();
        }

        public IList<Page> GetAll(int skip, int take, out int totalCount)
        {
            return _commonService.GetAll(skip, take, out totalCount);
        }

        public Page Get(int id)
        {
            return _commonService.Get(id);
        }

        public bool Delete(int id)
        {
            Page page = _commonService.Get(id);
            if (page != null)
            {
                return _commonService.Delete(page);
            }
            return false;
        }
    }
}