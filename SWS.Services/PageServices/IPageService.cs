﻿using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.PageServices
{
    public interface IPageService
    {
        bool Add(Page pageType);

        bool Update(Page pageType);

        IList<Page> GetAll();

        IList<Page> GetAll(int skip, int take, out int totalCount);

        Page Get(int id);

        bool Delete(int id);
    }
}
