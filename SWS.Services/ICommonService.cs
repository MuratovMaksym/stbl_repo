﻿
using System.Collections.Generic;

namespace SWS.Services
{
    interface ICommonService<T> where T : class
    {
        bool Add(T entity);

        bool Update(T entity);

        bool Delete(T entity);

        T Get(int id);

        IList<T> GetAll();

        IList<T> GetAll(int skip, int take, out int totalCount);
    }
}
