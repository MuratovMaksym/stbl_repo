﻿using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.PageTypeServices
{
    public interface IPageTypeService
    {
        bool Add(PageType pageType);

        bool Update(PageType pageType);

        IList<PageType> GetAll();

        IList<PageType> GetAll(int skip, int take, out int totalCount);

        PageType Get(int id);

        bool Delete(int id);
    }
}
