﻿using System.Collections.Generic;
using SWS.Common;

namespace SWS.Services.PageTypeServices
{
    public class PageTypeService : IPageTypeService
    {
        private readonly ICommonService<PageType> _commonService;
        private readonly SWSContext _swsContext;

        public PageTypeService(SWSContext dbContext)
        {
            _commonService = new CommonService<PageType>(dbContext);
            _swsContext = dbContext;
        }

        public bool Add(PageType pageType)
        {
            return _commonService.Add(pageType);
        }

        public bool Update(PageType pageType)
        {
            return _commonService.Update(pageType);
        }

        public IList<PageType> GetAll()
        {
            return _commonService.GetAll();
        }

        public IList<PageType> GetAll(int skip, int take, out int totalCount)
        {
            return _commonService.GetAll(skip, take, out totalCount);
        }

        public PageType Get(int id)
        {
            return _commonService.Get(id);
        }

        public bool Delete(int id)
        {
            PageType pageType = _commonService.Get(id);
            if (pageType != null)
            {
                return _commonService.Delete(pageType);
            }
            return false;
        }
    }
}
