﻿using System.Collections.Generic;
using System.Linq;
using SWS.Common;

namespace SWS.Services
{
    public class CommonService<T> : ICommonService<T> where T : class
    {
        private readonly SWSContext _dbContext;

        public CommonService(SWSContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        private bool Save()
        {
            return _dbContext.SaveChanges() > 0;
        }

        public bool Add(T entity)
        {
            _dbContext.Entry(entity).State = System.Data.Entity.EntityState.Added;
            return Save();
        }

        public bool Update(T entity)
        {
            _dbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            return Save();
        }

        public bool Delete(T entity)
        {
            _dbContext.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
            return Save();
        }

        public T Get(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public IList<T> GetAll()
        {
            return _dbContext.Set<T>().ToList();
        }

        public IList<T> GetAll(int skip, int take, out int totalCount)
        {
            totalCount = _dbContext.Set<T>().Count();
            return _dbContext.Set<T>().ToList();
        }
    }
}
