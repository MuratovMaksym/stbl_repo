//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SWS.Common
{
    using System;
    using System.Collections.Generic;
    
    public partial class PagedBlock
    {
        public int Id { get; set; }
        public int BlockId { get; set; }
        public int PageId { get; set; }
        public bool Active { get; set; }
    
        public virtual Block Block { get; set; }
        public virtual Page Page { get; set; }
    }
}
